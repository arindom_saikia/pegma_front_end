var myApp   			  = angular.module('myApp', []);
var selectedCategory 	  = null;
var selectedGenres        = null;
var slectedSubGenres      = null;

//  Force AngularJS to call our JSON Web Service with a 'GET' rather than an 'OPTION' 
//  Taken from: http://better-inter.net/enabling-cors-in-angular-js/
myApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);

myApp.controller('MasterDetailCtrl',
    function ($scope, $http) {
		
        $scope.listOfCustomers   = null;
		$scope.listOfGenres      = null;
		$scope.listOfSubGenres   = null;
		$scope.selectedCustomer  = null;
		$scope.selectedGenre     = null;
		$scope.slectedSubGenre     = null;
	

		$http({
        url: "http://localhost:9000/category",      
        method: 'GET'})
        .success(function(data){ 
		   $scope.errorMessage_POST = "Posted ";
		   $scope.listOfCustomers = data;
		   $scope.selectCustomer();
		  })
		  .error(function (status) {
		   console.log(data);
		   console.log("aRINDAM - GET-FAILED");
           $scope.errorMessage_POST = "pOST fAILED"
        });		
    
        $scope.selectCustomer = function () {
            $scope.selectedCustomer = $scope.listOfCustomers[0].categoryId;
			$selectedCategory = $scope.selectedCustomer;
            $scope.loadOrders();
        }
		
		$scope.selectGenre = function () {
            $scope.selectedGenre = $scope.listOfGenres[0].genreId;
			$selectedGenres = $scope.selectedGenre;
            $scope.loadSubGenres();
        }
		
		$scope.updateSubGenres = function() {
			$slectedSubGenres = $scope.slectedSubGenre;
		}
		

        $scope.loadOrders = function () {  
            $scope.listOfOrders = null;
            $http.get('http://localhost:9000/'+$scope.selectedCustomer+'/genre' )
                    .success(function (data) {
						$selectedCategory = $scope.selectedCustomer;
                        $scope.listOfGenres = data;
						$scope.selectGenre ();
                    })
                    .error(function (data, status, headers, config) {
                        $scope.errorMessage = "Couldn't load the list of Orders, error # " + status;
                    });
        }
		
		$scope.loadSubGenres = function () {
            $scope.listOfSubGenres = null;
            $http.get('http://localhost:9000/'+$scope.selectedCustomer+'/genre')
                    .success(function (data) {	
						$selectedGenres = $scope.selectedGenre;					
                        $scope.listOfSubGenres = data;
						$scope.slectedSubGenre  = $scope.listOfSubGenres[0].genreId;
						console.log($scope.slectedSubGenre);
						$slectedSubGenres = $scope.slectedSubGenre;
                    })
                    .error(function (data, status, headers, config) {
                        $scope.errorMessage = "Couldn't load the list of Orders, error # " + status;
                    });
        }
		
    });
	